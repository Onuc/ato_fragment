package com.example.technical.fragment;

import android.support.v4.app.Fragment;

/**
 * Created by Technical on 1/31/2016.
 */
public class TitledFragment extends Fragment {
    protected String title;

    protected String getTitle() {
        return this.title;
    }

    protected void setTitle(String title) {
        this.title = title;
    }
}

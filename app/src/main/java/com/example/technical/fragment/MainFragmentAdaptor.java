package com.example.technical.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

class MainFragmentAdaptor extends FragmentPagerAdapter {
    private ArrayList<TitledFragment> fragments = new ArrayList<>();

    public MainFragmentAdaptor(FragmentManager fm) {
        super(fm);
        this.fragments.add(new WelcomeFragment());
        this.fragments.add(new AboutFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }
}

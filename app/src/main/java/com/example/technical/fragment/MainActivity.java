package com.example.technical.fragment;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {
    private ActionBar actionBar;
//    private String[] actionBarTabs;
    private ViewPager viewPager;
//    private ArrayList<ActionBar.Tab> tabs = new ArrayList<ActionBar.Tab>();
    private MainFragmentAdaptor fragmentAdaptor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        this.actionBar = this.getActionBar();
//        this.actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        setContentView(R.layout.activity_main);
//        this.init();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void init() {
        this.viewPager = (ViewPager) this.findViewById(R.id.main_view_pager);
        this.fragmentAdaptor = new MainFragmentAdaptor(this.getSupportFragmentManager());
        this.viewPager.setAdapter(this.fragmentAdaptor);

        for (int i = 0; i < this.fragmentAdaptor.getCount(); i++) {
            TitledFragment currentTitledFragment = (TitledFragment) this.fragmentAdaptor.getItem(i);
            ActionBar.Tab tab = this.actionBar.newTab().setText(currentTitledFragment.getTitle());
            tab.setTabListener(new ActionBar.TabListener() {
                @Override
                public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                    MainActivity.this.viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

                }

                @Override
                public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

                }
            });
        }

        this.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                MainActivity.this.actionBar.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
}
